
<!-- README.md is generated from README.Rmd. Please edit that file -->

# commitr

[![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/gl/ntrlshrp/commitr/master?urlpath=rstudio)

This RStudio Addin prompts the user for high-quality commits that
semantic release software use to:

  - determine the next semantic version using the previous version and
    commit messages
  - create/update changelog
  - create tags with release notes
  - attach files to release notes
  - commit version bumps.

Currently supported semantic release software includes:
`go-semrel-gitlab` (<https://juhani.gitlab.io/go-semrel-gitlab/>).

### How to cite

Please cite this Addin as:

> Authors, (2019). *commitr*. Accessed 13 Oct 2019. Online at
> <https://doi.org/xxx/xxx>

### How to download or install

You can download the Addin as a zip from from this URL:
</archive/master.zip>

Or you can install this Addin as an R package, commitr, from GitLab
with:

``` r
# install.packages("devtools")
remotes::install_gitlab("ntrlshrp/commitr")
```

### Licenses

**Text and figures :**
[CC-BY-4.0](http://creativecommons.org/licenses/by/4.0/)

**Code :** See the [DESCRIPTION](DESCRIPTION) file

**Data :** [CC-0](http://creativecommons.org/publicdomain/zero/1.0/)
attribution requested in reuse

### Contributions

We welcome contributions from everyone. Before you get started, please
see our [contributor guidelines](CONTRIBUTING.md). Please note that this
project is released with a [Contributor Code of Conduct](CONDUCT.md). By
participating in this project you agree to abide by its terms.
