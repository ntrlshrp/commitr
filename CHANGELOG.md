# CHANGELOG

<!--- next entry here -->

## 0.1.3
2019-12-28

### Fixes

- **ci/cd:** Update go-semrel-gilab version and add code coverage job (5e111f71167ec65a362f6bc8e09045e29393ab4d)

## 0.1.2
2019-10-14

### Fixes

- **ci/cd:** Added "check-package" (483374bb5dba030511625eee763556657d3b15bd)
- **ci/cd:** Added "check-package" to .gitlab-ci.yml (e70554781cb5c27691f9b02b0b95cf0784a87cbe)
- **imports:** Added `usethis` to Imports (98b9186cd838d08ce1c5320c435334f78729ecce)
- **r package:** Added generic package file docs (c83df89a87a27e04990e4fef8d9f0675868f477d)
- **check-package:** Added .gitlab-ci.yml to .Rbuildignore, added namespaces to commitr.R (bc7834f5986419b314f49323055bdfd1d5e80b05)

## 0.1.1
2019-10-13

### Fixes

- **readme.md:** Updated template with specifics (9ac4e646470d166468e8938de6a7ab2bb92b0563)

## 0.1.0
2019-10-13

### Features

- **r package:** Added DESCRIPTION, NAMESPACE, LICENSE.md, .Rbuildignore, inst/rstudio/addins.dcf (73b43b278d18efd82fa5bb5f18b646dc33cac02e)
- **commitsemanticrelease:** Created RStudio Addin (1d672e6b03a0a40096392041a9b95b7f009cd99c)
- **ci/cd:** Added .gitlab-ci.yml (3d99533eab0d745c022e9d3edaf93becbc3619d3)
- **go-semrel-gitlab:** Provided .gitlab-ci.yml template (3db3311997da45c86504cf972a38fe08f30ae4b9)